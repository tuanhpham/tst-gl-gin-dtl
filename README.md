# Testing datalad

## Requirements

- `git`
- `git-annex`
- `datalad`
- `datalad-next`

## Start

```bash
datalad create -c text2git <PROJECT>
cd <PROJECT>

# Set author
git config user.name <NAME>
git config user.email <GITLAB-EMAIL>            

# Gitlab repo, ignore annex
git remote add origin git@gitlab.com:<GITLAB-USERNAME>/<PROJECT>.git
git config remote.origin.annex-ignore true 

# Gin repo
datalad siblings add -d . \
  --name gin \
  --url git@gin.g-node.org:/<GIN-USERNAME>/<PROJECT>.git
```

## When working

In progress, only use `datalad save` with **a specified data folder** (see TODOs), optionally tag data version

```bash
datalad save \
  -m '[datalad] add data/sub2' \ # this is for commit mesg
  --version-tag 'data.sub2' \ # optional
  data # REQUIRED FOR THE PURPOSE OF TRACKING ONLY DATA BY GIT-ANNEX
```

In case need to overwrite a file directly, sometimes would need to unlock first

## To push

Do both `git push` and `datalad push --to gin`. 

For tagging, can do `git push --tags` but unclear whether there's a special arg with `datalas push`

## When "pulling"

For cloning in CI:

```bash
# ASSUMED: inside the cloned repo from CI clone
# e.g. git clone https://gitlab.com/tuanhpham/tst-gl-gin-dtl
# ASSUMED: datalad is already installed
datalad siblings add -d . \
  --name gin \
  --url https://gin.g-node.org/<GIN-USERNAME>/<PROJECT>
  # NOTE: there is NO ".git" at the end
git config remote.gin.annex-ignore false
datalad update

# Then get specific folders/files
datalad get data
```

During testing or whatever, after cloning, if need to delete, because the `git/annex` folder somehow has permission very strict, just use `sudo` on local. For CI, I don't think it would matter for the most parts.

## Branching

- With new branches, `push` on the `git` doesn't change, on the `datalad` side still nothing notable
- When doing `git pull` then checkout might need to do this to stick with the Gitlab remote version `git checkout --track origin/<NEW-BRANCH>`. Then for the `datalad` side, need to do `datalad update` then `datalad get data`

## TODO / To-be-questioned

- [ ] See if possible to co-author and sign with GIN username
- [ ] Unclear how to set `.gitattributes` properly to force `datalad save/status` to ignore most folders except `data`
- [ ] When pushing to GIN, `datalad push` doesn't push tags. Unclear if side effects when doing `git push --tags` afterwards
- [ ] Unclear if [this](https://brainhack-princeton.github.io/handbook/content_pages/03-06-sampleProjectWithDatalad.html) i  a better approach: no-annex first then datalad for subdataset (I think that's what they did?)
